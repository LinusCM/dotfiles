# Dotfiles.
This repo contains my personal Nix configuration, built around Home Manager and tailored towards development.


## Before install
If you're not using NixOS, start out with installing Nix.

This can be done with a *multi-user* installation (recommended):
```sh
sh <(curl -L https://nixos.org/nix/install) --daemon
```

Or if you are running WSL, a *single-user* installation:
```sh
sh <(curl -L https://nixos.org/nix/install) --no-daemon
```

### Nix channels
The unstable and Home Manager channels can now be added:
```sh
# Nixpkgs unstable, if you're not using NixOS
nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs

# NixOS unstable, if you are.
nix-channel --add https://nixos.org/channels/nixos-unstable nixos

# Home Manager, use sudo if you're on NixOS.
nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager

# Update channels, again with sudo on NixOS.
nix-channel --update
```

## Setup script
Make sure the repo is cloned to the home directory of the user you wish to install the configuration for.

Then in `./nixpkgs/common.nix` change `user` to fit your choice.

Install Home Manager:
```sh
nix-shell '<home-manager>' -A install
```

The setup script can now be run. The alias `us` (update system) can be used in the future
to update the configuration/packages.

## Setup Nushell for non-NixOS installations
As Nushell is installed with home-manager, the path of the shell has to manually be added to the `/etc/shell` file:
```sh
sudo sh -c 'which nu >> /etc/shells'
```

Then the default shell can be set with:
```sh
chsh -s $(which nu)
```

## Auto mounting extra drives
To mount extra drives, mount them as usual:
```sh
mount /dev/sdx1 /mnt/folder_name
```

Then run `nixos-generate-config`. This will replace `/etc/nixos/hardware-configuration.nix`.

The config can now be rebuilt:
```sh
nixos-rebuild switch
```

## Physical MFA
To enable the use of physical authentication keys:
```sh
nix-shell -p pam_u2f
mkdir -p ~/.config/keys
pamu2fcfg > ~/.config/keys/u2f_keys

# Add another key:
pamu2fcfg -n >> ~/.config/keys/u2f_keys
```
