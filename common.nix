{
  user = "lm";
  system = "bimdesk";

  colorscheme = {
    # Programs
    btop = "gruvbox_dark_v2";

    # Theme made by Sainnhe
    # Git: https://github.com/sainnhe/gruvbox-material
    colors = {
      # Background
      bg0 = "#101010";
      bg1 = "#1c1c1c";
      bg = "#292828";
      bg2 = "#32302f";
      bg3 = "#383432";
      bg4 = "#3c3836";
      bg5 = "#45403d";
      bg6 = "#504945";
      bg7 = "#5a524c";
      bg8 = "#665c54";
      bg9 = "#7c6f64";
      gray0 = "#7c6f64";
      gray1 = "#928374";
      gray2 = "#a89984";

      # Foreground
      fg0 = "#ddc7a1";
      fg = "#d4be98";
      fg1 = "#c5b18d";

      # Regular
      red = "#ea6962";
      orange = "#e78a4e";
      yellow = "#d8a657";
      green = "#a9b665";
      aqua = "#89b482";
      blue = "#7daea3";
      purple = "#d3869b";

      # Light
      lightred = "#ec7872";
      lightorange = "#e8975a";
      lightyellow = "#daa76b";
      lightgreen = "#bac76f";
      lightaqua = "#a2c788";
      lightblue = "#95c9a6";
      lightpurple = "#d499af";
    };
  };
}
