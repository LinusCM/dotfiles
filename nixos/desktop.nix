{pkgs, ...}: {
  # Desktop environment
  services.xserver = {
    enable = true;

    # Gnome
    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;

    # Disable Xterm
    excludePackages = [pkgs.xterm];
    desktopManager.xterm.enable = false;
  };

  # Extensions
  environment.systemPackages = with pkgs; [
    gnomeExtensions.caffeine
  ];

  # Remove Gnome bloat
  environment.gnome.excludePackages = with pkgs; [
    yelp
    epiphany
    geary
    evince
    totem
    file-roller
    simple-scan
    baobab
    gedit

    gnome-tour
    gnome-console
    gnome-text-editor
    gnome-connections
    gnome-calculator
    gnome-system-monitor
    gnome-font-viewer
    gnome-music
    gnome-characters
    gnome-weather
    gnome-maps
    gnome-logs
    gnome-contacts
  ];
}
