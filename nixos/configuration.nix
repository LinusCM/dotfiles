let
  common = import ./common.nix;

  user = common.user;
  system = common.system;
in
  {pkgs, ...}: {
    imports = [
      # Config
      ./desktop.nix
      ./camera.nix
      ./turntable.nix

      # General
      ./hardware-configuration.nix
      <home-manager/nixos>
    ];

    # Bootloader
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;

    # Networking
    networking = {
      networkmanager.enable = true;
      hostName = "${user}-${system}";
      nameservers = ["1.1.1.1" "1.0.0.1"];
    };

    # Time zone
    time.timeZone = "Europe/Copenhagen";

    # Internationalisation properties
    i18n.defaultLocale = "en_US.UTF-8";
    i18n.extraLocaleSettings = {
      LC_ADDRESS = "en_US.UTF-8";
      LC_IDENTIFICATION = "en_US.UTF-8";
      LC_MEASUREMENT = "en_US.UTF-8";
      LC_MONETARY = "en_US.UTF-8";
      LC_NAME = "en_US.UTF-8";
      LC_NUMERIC = "en_US.UTF-8";
      LC_PAPER = "en_US.UTF-8";
      LC_TELEPHONE = "en_US.UTF-8";
      LC_TIME = "en_US.UTF-8";
    };

    users = {
      # Shell
      defaultUserShell = pkgs.nushell;

      # User config
      users.lm = {
        isNormalUser = true;
        description = user;
        extraGroups = ["networkmanager" "wheel"];
      };
    };

    security = {
      rtkit.enable = true;

      # Physical MFA
      pam.services.login.u2fAuth = true;
      pam.services.sudo.u2fAuth = true;
    };

    services = {
      # Sound
      pulseaudio.enable = false;
      pipewire = {
        enable = true;
        alsa.enable = true;
        alsa.support32Bit = true;
        pulse.enable = true;
      };

      # X11 Keymap
      xserver.xkb = {
        layout = "us";
        variant = "";
      };

      # VPN
      mullvad-vpn.enable = true;

      # Syncthing
      syncthing = {
        enable = true;
        user = user;
        configDir = "/home/${user}/.config/syncthing";
      };
    };

    # Packages
    nixpkgs.config.allowUnfree = true;

    environment.systemPackages = with pkgs; [
      home-manager
    ];

    programs.steam.enable = true;

    # Fonts
    fonts.packages = with pkgs; [
      intel-one-mono
    ];

    # Version
    system.stateVersion = "23.05";
  }
