{pkgs, ...}: {
  systemd.user.services.turntable = {
    description = "Virtual Turntable Audio Device";
    after = ["pipewire.service"];
    serviceConfig = {
      ExecStart = "${pkgs.pipewire}/bin/pw-loopback --capture-props=node.name=Turntable_Source --playback-props=node.name=Turntable_Sink";
      Restart = "on-failure";
      StandardOutput = "journal";
      StandardError = "journal";
    };
    wantedBy = ["default.target"];
  };
}
