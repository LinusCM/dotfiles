{pkgs, ...}: {
  home.packages = with pkgs; [
    # Rust
    rustc
    rust-analyzer
    rustfmt
    cargo

    # Nix
    nil
    alejandra

    # Go
    go
    gopls
    gofumpt

    # C
    gcc
    clang-tools

    # .NET
    dotnet-sdk_8
    csharp-ls

    # Java/Kotlin
    jdk
    jdt-language-server

    kotlin
    kotlin-language-server
    ktlint

    gradle

    # Python
    (
      let
        my-python-packages = python-packages:
          with python-packages; [
            uv
            debugpy
          ];
        python-with-my-packages = python313.withPackages my-python-packages;
      in
        python-with-my-packages
    )

    ruff
    pyright
    poetry

    # Lua
    lua
    lua-language-server

    # Bash
    bash-language-server
    shfmt

    # Nu
    nufmt

    # SQL
    sqls

    # JavaScript/TypeScript
    deno
    nodejs
    nodePackages.typescript-language-server

    # MISC frontend
    nodePackages.vscode-langservers-extracted
    nodePackages.prettier
    svelte-language-server
  ];

  # Dev environment
  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };

  # Git
  programs.git = {
    enable = true;
    difftastic.enable = true;
    difftastic.display = "inline";

    extraConfig = {
      push.autoSetupRemote = true;
    };
  };
}
