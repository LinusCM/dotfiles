{pkgs, ...}: {
  home.packages = with pkgs; [
    # Multimedia
    celluloid
    gapless

    # Browser
    librewolf

    # General
    krita
    discord

    # Games
    lutris
    prismlauncher
  ];
}
