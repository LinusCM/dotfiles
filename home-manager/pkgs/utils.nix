{pkgs, ...}: {
  home.packages = with pkgs; [
    # Misc
    ripgrep
    tio

    # Shell
    nushellPlugins.highlight
    nushellPlugins.polars
    nushellPlugins.query

    # HTTP
    curl
    wget

    # Fetch
    onefetch
    fastfetch

    # Archive
    zip
    unzip

    # Camera
    ffmpeg_6-full
    gphoto2
    v4l-utils
  ];
}
