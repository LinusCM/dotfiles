{
  imports = [
    ./shell.nix
    ./zellij.nix
    ./helix
    ./lsps
    ./btop.nix
  ];
}
