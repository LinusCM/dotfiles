{
  programs.ruff = {
    enable = true;

    settings.lint = {
      pydocstyle.convention = "google";

      per-file-ignores = {"__init__.py" = ["F401"];};

      lake8-annotations = {
        allow-star-arg-any = true; # Allows untyped *args and **kwargs.
        suppress-dummy-args = true; # Ignores type annotations from dummy arguments.
        suppress-none-returning = true; # Allows functions that don't return anything to not hint return type.
      };

      select = [
        "E"
        "W"
        "C901" # McCabe complexity check.
        "F" # Pyflakes error analysis.
        "N" # PEP8 naming convention check.
        "Q" # Flake8-quotes, checks for quote and string related issues.
        "SLF" # Checks for private member access.
        "SIM" # Checks for code that can be easily simplified.
        "B" # Flake8-bugbear which looks for likely bugs and design problems.
        "RUF" # Rules and checks that are specific to Ruff.
        "FURB" # Checks for code that should be refurbished, due to using old Python conventions.
        "UP" # Checks for outdated and deprecated code.
        "RET" # Checks for useless returns or assignments in functions.
        "FA100" # Incorrect use of typing check.
        "INP" # Checks for missing __init__.py files in namespace packages.
        "PL" # Pylint, checks for code smells and ensures that we follow convention.
        "ANN" # Ensures type annotation.
      ];

      extend-ignore = [
        "E501" # Prevents warnings about lines being too long.
        "N818" # Allows custom exceptions to not have the error suffix.
        "B006" # Allows mutable data structures as default arguments.
        "RUF015" # Doesn't care if you use single element slice or next({iterable}).
        "D202" # Stops caring if you put a blank line after docstring.
        "D100" # Allows python files without a docstring at the top of it.
        "D101" # Allows undocumented public classes.
        "D105" # Allows undocumented magic methods.
        "D107" # Allows undocumented constructors.
        "RET503" # Stops forcing explicit return statement on functions that can return multiple types.
        "PLR0913" # Stops forcing a maximum amount of function arguments.
        "PLW2901" # Allows outer variables to be overwritten by an inner loop.
        "PLR2004" # Allows the use of magic variables.
        "ANN101" # Allows (self) argument to not have type annotation.
        "ANN401" # Allows the use of the Any type in type annotations.
      ];
    };
  };
}
