{
  programs = {
    # Shell
    nushell = {
      enable = true;

      extraConfig = ''
        $env.config = {
          show_banner: false,
          use_kitty_protocol: true
          bracketed_paste: true

          completions: {
            case_sensitive: false
            quick: true
            partial: true
          }

          display_errors: {
            exit_code: true
          }

          history: {
            file_format: 'sqlite'
            max_size: 5_000_000
            isolation: true
            sync_on_enter: true
          }
        }

        # Functions
        def us [] {
          let currentDir = (pwd)
          cd ~/dotfiles
          git pull -q
          ./setup
          cd $currentDir
        }

        def zjc [argv] {
          command zellij run --name "$argv" --floating -- nu -c "$argv"
        }

        # Misc Variables
        $env.MOZ_ENABLE_WAYLAND = '1'
        $env.STARSHIP_SHELL = "nu"
        $env.GPG_TTY = (tty)

        # Misc paths
        $env.PATH = ($env.PATH | split row (char esep) | prepend ["~/.nix-profile/bin/" "/usr/local/bin" "/usr/bin"] | append ~/.dotnet/tools)
        $env.GOPATH = ($env.HOME) + "/.go"

        # Starship
        $env.TRANSIENT_PROMPT_COMMAND = ^starship module character
        $env.TRANSIENT_PROMPT_INDICATOR = ""
        $env.TRANSIENT_PROMPT_INDICATOR_VI_INSERT = ""
        $env.TRANSIENT_PROMPT_INDICATOR_VI_NORMAL = ""
        $env.TRANSIENT_PROMPT_MULTILINE_INDICATOR = ""

        # Editor
        $env.VISUAL = "~/.nix-profile/bin/hx"
        $env.EDITOR = "~/.nix-profile/bin/hx"
        $env.SUDO_EDITOR = "~/.nix-profile/bin/hx"
      '';

      shellAliases = {
        # Misc
        l = "ls";
        ll = "ls -la";
        c = "clear";
        np = "nix-shell --command nu -p";

        # Zellij
        zj = "zellij -s";
        zja = "zellij attach";
        zjd = "zellij delete-session";

        # Camera
        fuji = "bash -c \"gphoto2 --stdout --capture-movie | ffmpeg -i - -vcodec rawvideo -pix_fmt yuv420p -threads 0 -f v4l2 /dev/video0 &>/dev/null &\"";
        fujik = "pkill -f \"gphoto2 --stdout --capture-movie\"";
      };
    };

    # Completion
    carapace.enable = true;
    carapace.enableNushellIntegration = true;

    # Prompt
    starship = {
      enable = true;

      settings = {
        character = {
          success_symbol = "[>](bold green)";
          error_symbol = "[>](bold red)";
        };

        add_newline = false;
      };
    };
  };
}
