let
  common = import ../../common.nix;
  colors = common.colorscheme.colors;
in {
  programs.ghostty = {
    enable = true;

    settings = {
      theme = "gruvbox-material";
      font-size = 14;
    };

    themes = {
      gruvbox-material = {
        palette = [
          "0=${colors.bg0}"
          "1=${colors.red}"
          "2=${colors.green}"
          "3=${colors.yellow}"
          "4=${colors.blue}"
          "5=${colors.purple}"
          "6=${colors.aqua}"
          "7=${colors.gray1}"
          "8=${colors.bg8}"
          "9=${colors.lightred}"
          "10=${colors.lightgreen}"
          "11=${colors.lightyellow}"
          "12=${colors.lightblue}"
          "13=${colors.lightpurple}"
          "14=${colors.lightaqua}"
          "15=${colors.fg1}"
        ];
        background = colors.bg;
        foreground = colors.fg;
        cursor-color = colors.fg0;
        selection-background = colors.bg2;
        selection-foreground = colors.fg0;
      };
    };
  };
}
