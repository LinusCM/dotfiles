let
  common = import ../../common.nix;
  colors = common.colorscheme.colors;
in {
  # Helix theme made by emil-s
  # Git: https://gitlab.com/emil-s
  programs.helix.themes = {
    custom_theme = {
      "type" = colors.blue;
      "constant" = colors.purple;
      "constant.builtin" = colors.purple;
      "constant.character.escape" = colors.yellow;
      "constant.numeric" = colors.purple;
      "string" = colors.green;
      "string.regexp" = colors.blue;
      "comment" = colors.gray0;
      "variable" = colors.fg0;
      "variable.builtin" = colors.purple;
      "variable.parameter" = colors.fg0;
      "variable.other.member" = colors.yellow;
      "label" = colors.aqua;
      "punctuation" = colors.gray2;
      "punctuation.delimiter" = colors.gray2;
      "punctuation.bracket" = colors.gray2;
      "punctuation.special" = colors.yellow;
      "keyword" = colors.red;
      "keyword.directive" = colors.aqua;
      "keyword.storage.modifier" = colors.orange;
      "operator" = colors.orange;
      "function" = colors.green;
      "function.method" = colors.aqua;
      "function.builtin" = colors.green;
      "function.macro" = colors.green;
      "tag" = colors.yellow;
      "namespace" = colors.aqua;
      "attribute" = colors.blue;
      "constructor" = colors.yellow;
      "module" = colors.blue;
      "special" = colors.orange;

      "diff.plus" = colors.lightgreen;
      "diff.minus" = colors.lightred;
      "diff.delta" = colors.lightorange;
      "diff.delta.moved" = colors.lightblue;

      "markup.heading.marker" = colors.gray2;
      "markup.heading.1" = {
        fg = colors.red;
        modifiers = ["bold"];
      };
      "markup.heading.2" = {
        fg = colors.orange;
        modifiers = ["bold"];
      };
      "markup.heading.3" = {
        fg = colors.yellow;
        modifiers = ["bold"];
      };
      "markup.heading.4" = {
        fg = colors.green;
        modifiers = ["bold"];
      };
      "markup.heading.5" = {
        fg = colors.blue;
        modifiers = ["bold"];
      };
      "markup.heading.6" = {
        fg = colors.fg0;
        modifiers = ["bold"];
      };
      "markup.list" = colors.red;
      "markup.bold" = {modifiers = ["bold"];};
      "markup.italic" = {modifiers = ["italic"];};
      "markup.link.url" = {
        fg = colors.blue;
        modifiers = ["underlined"];
      };
      "markup.link.text" = colors.purple;
      "markup.quote" = colors.gray2;
      "markup.raw" = colors.green;

      "ui.background.separator" = colors.gray0;

      "ui.cursor" = {
        fg = colors.bg0;
        bg = colors.fg1;
      };
      "ui.cursor.primary" = {
        fg = colors.bg0;
        bg = colors.fg0;
      };
      "ui.cursor.match" = {
        fg = colors.orange;
        bg = colors.gray2;
      };
      "ui.cursor.insert" = {
        fg = colors.bg0;
        bg = colors.gray2;
      };
      "ui.cursor.select" = {
        fg = colors.bg0;
        bg = colors.blue;
      };

      "ui.cursorline.primary" = {bg = colors.bg4;};
      "ui.cursorline.secondary" = {bg = colors.bg;};

      "ui.selection" = {bg = colors.bg3;};

      "ui.linenr" = colors.gray0;
      "ui.linenr.selected" = colors.fg0;

      "ui.statusline" = {
        fg = colors.fg0;
        bg = colors.bg3;
      };
      "ui.statusline.inactive" = {
        fg = colors.gray0;
        bg = colors.bg2;
      };
      "ui.statusline.normal" = {
        fg = colors.bg0;
        bg = colors.blue;
        modifiers = ["bold"];
      };
      "ui.statusline.insert" = {
        fg = colors.bg0;
        bg = colors.green;
        modifiers = ["bold"];
      };
      "ui.statusline.select" = {
        fg = colors.bg0;
        bg = colors.purple;
        modifiers = ["bold"];
      };

      "ui.bufferline" = {
        fg = colors.gray0;
        bg = colors.bg1;
      };
      "ui.bufferline.active" = {
        fg = colors.fg0;
        bg = colors.bg3;
        modifiers = ["bold"];
      };

      "ui.popup" = {
        fg = colors.gray2;
        bg = colors.bg2;
      };
      "ui.window" = {
        fg = colors.gray0;
        bg = colors.bg2;
      };
      "ui.help" = {
        fg = colors.fg0;
        bg = colors.bg2;
      };

      "ui.text" = colors.fg0;
      "ui.text.focus" = colors.fg0;

      "ui.menu" = {
        fg = colors.fg0;
        bg = colors.bg3;
      };
      "ui.menu.selected" = {
        fg = colors.bg0;
        bg = colors.blue;
        modifiers = ["bold"];
      };
      "ui.menu.scroll" = {
        fg = colors.fg1;
        bg = colors.bg;
      };

      "ui.virtual.whitespace" = {fg = colors.bg4;};
      "ui.virtual.indent-guide" = {fg = colors.bg4;};
      "ui.virtual.ruler" = {bg = colors.bg3;};
      "ui.virtual.inlay-hint" = {fg = colors.gray0;};
      "ui.virtual.jump-label" = {
        bg = colors.purple;
        fg = colors.bg0;
        modifiers = ["bold"];
      };

      "hint" = colors.blue;
      "info" = colors.aqua;
      "warning" = colors.yellow;
      "error" = colors.red;
      "diagnostic" = {modifiers = ["underlined"];};
    };
  };
}
