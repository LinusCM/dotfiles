{
  imports = [
    ./languages.nix
    ./theme.nix
  ];

  programs.helix = {
    enable = true;
    settings = {
      theme = "custom_theme";

      editor = {
        scrolloff = 9;
        true-color = true;
        color-modes = true;
        cursorline = true;
        gutters = ["diagnostics" "line-numbers"];
        line-number = "relative";
        soft-wrap.enable = true;
        bufferline = "multiple";
        jump-label-alphabet = "ntesiroahdmgywuflpkv";

        # Autocomplete
        idle-timeout = 50;
        completion-trigger-len = 1;

        cursor-shape = {
          normal = "block";
          insert = "bar";
          select = "underline";
        };

        statusline = {
          left = ["mode" "spinner" "file-name"];
          right = ["diagnostics" "position"];
        };

        lsp = {
          display-inlay-hints = true;
          display-messages = true;
        };
      };
    };
  };
}
