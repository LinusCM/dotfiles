{
  programs.helix.languages = {
    language-server = {
      # Nix
      nil.command = "nil";

      # Python
      pyright = {
        command = "pyright-langserver";
        args = ["--stdio" "--lib" "-p" "."];
        config = {};
      };

      ruff = {
        command = "ruff";
        args = ["server"];
      };
    };

    language = [
      # Nix
      {
        name = "nix";
        formatter = {
          command = "alejandra";
          args = ["--quiet"];
        };
        auto-format = true;
      }

      # C
      {
        name = "c";
        auto-format = true;
        language-servers = ["clangd"];
      }

      # C Sharp
      {
        name = "c-sharp";
        auto-format = true;
        language-servers = ["csharp-ls"];
      }

      # Kotlin
      {
        name = "kotlin";
        formatter = {
          command = "ktlintFormat";
        };
        auto-format = true;
      }

      # Python
      {
        name = "python";
        auto-format = true;
        language-servers = [
          {
            name = "ruff";
            only-features = ["format" "diagnostics" "code-action"];
          }
          {
            name = "pyright";
            except-features = ["format"];
          }
        ];
        roots = ["pyproject.toml" "setup.py" "Poetry.lock" "."];
      }

      # Lua
      {
        name = "lua";
        auto-format = true;
      }

      # Javascript
      {
        name = "javascript";
        formatter = {
          command = "prettier";
          args = ["--parser" "typescript"];
        };
        auto-format = true;
      }

      # Typescript
      {
        name = "typescript";
        formatter = {
          command = "prettier";
          args = ["--parser" "typescript"];
        };
        auto-format = true;
      }

      # Bash
      {
        name = "bash";
        indent = {
          tab-width = 4;
          unit = "    ";
        };
        formatter = {
          command = "shfmt";
          args = ["-i" "4"];
        };
        auto-format = true;
      }

      # HTML
      {
        name = "html";
        formatter = {
          command = "prettier";
          args = ["--parser" "html"];
        };
      }

      # CSS
      {
        name = "css";
        formatter = {
          command = "prettier";
          args = ["--parser" "css"];
        };
        auto-format = true;
      }

      # JSON
      {
        name = "json";
        formatter = {
          command = "prettier";
          args = ["--parser" "json"];
        };
      }

      # Svelte
      {
        name = "svelte";
        auto-format = true;
      }
    ];
  };
}
