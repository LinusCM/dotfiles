let
  common = import ../common.nix;
  theme = common.colorscheme.btop;
in {
  programs.btop = {
    enable = true;
    settings = {
      color_theme = theme;
      theme_background = false;

      rounded_corners = false;
    };
  };
}
