let
  common = import ../common.nix;
  colors = common.colorscheme.colors;
in {
  programs.zellij = {
    enable = true;
    settings = {
      theme = "custom_theme";

      default_layout = "compact";
      pane_frames = false;

      themes.custom_theme = {
        fg = colors.fg;
        bg = colors.bg;

        black = colors.bg1;
        red = colors.red;
        green = colors.green;
        yellow = colors.yellow;
        blue = colors.blue;
        magenta = colors.purple;
        cyan = colors.aqua;
        white = colors.fg1;
        orange = colors.orange;
      };
    };
  };
}
